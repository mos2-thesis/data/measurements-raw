#!/usr/bin/perl

###--- DEME-TFSI TEST SCRIPT ---###
# This script can be used to test a pair of contacts on a DEME-TFSI sample. 
# A constant liquid-gate voltage is applied, while the bias voltage is swept.
#

#--- 0. Import Packages ---#
use Lab::Moose;
use Lab::Moose::Countdown;
use Time::HiRes qw /time/;
use warnings;
use strict;
use Getopt::Long qw< :config auto_version bundling no_ignore_case >;
use Pod::Usage;

#--- 1. Set Parameters ---#
my $sensitivity = -1e-8;
my $liveplot = 1;
my $NPLC = 1;
my $id = int(rand(1e4));

#--- Parameter Input ---#
my $sample = "SAMPLE";
my $contact_1 = 0;
my $contact_2 = 0;
my $liquid_voltage = 0;
GetOptions(
  "sample=s"   => \$sample,      # string
  "contact1=i" => \$contact_1,
  "contact2=i" => \$contact_2,
  "liquid-voltage=i" => \$liquid_voltage,
  "sens=s" => \$sensitivity,
  "plot=i" => \$liveplot,
  
  # Standard options
  'usage'  => sub { pod2usage(2) },
  'help'   => sub { pod2usage(1) }
) or pod2usage(2);

#--- 2. Init Instruments ---#

##--- Ionic Liquid ---##
my $ionic = instrument(
  type => 'Keithley2400',
  connection_type => 'LinuxGPIB',
  connection_options => {gpib_address => 29},
  # mandatory protection setting2
  max_units_per_step => 0.0001, # max step is 1mV/1mA
  max_units_per_second => 0.002,
  min_units => -5,
  max_units => 5,
);

$ionic -> source_function(value => 'VOLT');
$ionic -> source_range(value => 3);
$ionic -> sense_function_on(value => ['CURR']);

##--- Device Bias ---##
my $device_bias = instrument(
  type => 'Yokogawa7651',
  connection_type => 'LinuxGPIB',
  connection_options => {gpib_address => 24},
  max_units_per_step => 0.0001,
  max_units_per_second => 0.001,
  min_units => -10,
  max_units => 10,
);

my $device_multimeter = instrument(
  type => 'Agilent34410A',
  connection_type => 'LinuxGPIB',
  connection_options => {pad => 15}
);

##--- Reference ---##
my $reference_multimeter = instrument(
	type => 'Agilent34410A',
	connection_type => 'LinuxGPIB',
	connection_options => {pad => 11}
);

#--- 3. Create Datafile and Plot ---#
my $folder = datafolder(path => $sample.'_'.$contact_1.'_'.$contact_2.'_2point_test');
my $datafile = datafile(
  folder => $folder,
  filename => $sample.'_'.$contact_1.'_'.$contact_2.'_2point_test',
  columns => [qw/id time gate_voltage reference_voltage gate_current bias_voltage device_current contact_1 contact_2/]
);

$datafile -> add_plot(
  x => 'bias_voltage',
  y => 'device_current',
  plot_options => {
	  title => "DEME-TFSI Test - $sample ($contact_1 / $contact_2)",
	  xlabel => 'Bias Voltage [V]',
	  ylabel => 'Device Current [A]',
	  grid => 0
  },
  curve_options => {
	  with => 'lines'
  }
);

#--- 6. Run ---#

$ionic -> set_level(value => $liquid_voltage);

countdown(10 * 60);

my @bias_values = linspace(from => 0, to => 0.1, step => 0.005);
push(@bias_values, linspace(from => 0.1, to => 0, step => 0.005));

for my $bias_value (@bias_values) {
  $device_bias -> set_level(value => $bias_value);

  my $gate_voltage = $ionic -> cached_level(),
  my $reference_voltage = $reference_multimeter -> get_value(),
  my $gate_current = $ionic -> get_measurement() -> {CURR};
  my $bias_voltage = $device_bias -> cached_level(),
  my $device_current = ($device_multimeter -> get_value()) * $sensitivity;

  $datafile -> log(
	id => $id,
    time => time(),
    gate_voltage => $gate_voltage,
    reference_voltage => $reference_voltage,
    gate_current => $gate_current,
    bias_voltage => $bias_voltage,
    device_current => $device_current,
    contact_1 => $contact_1,
    contact_2 => $contact_2
  );
}

# Set everything to zero
$device_bias -> set_level(value => 0);
$ionic -> set_level(value => 0);

__END__

=head1 NAME

deme-test-2point - DEME-TFSI 2-point test

=head1 SYNOPSIS

=for pod2usage:

B<deme-test-2point> [I<OPTIONS>]

=head1 OPTIONS

=for pod2usage:

=over

=item B<--sample>

Set the sample used.

=item B<--contact1>

Set the first contact.

=item B<--contact2>

Set the second contact.

=item B<--liquid-voltage>

Set the liquid-gate voltage.

=item B<--sens>

Set the sensitivity used (default=-1e-8).

=item B<--plot>

Enable/Disable live plotting (default=1).

=item B<--help>

Show this help text.

=item B<--usage>

=back

=head1 DESCRIPTION

...

=cut

