#!/usr/bin/perl
#PODNAME: deme-decay.pl
#ABSTRACT: Measure DEME current decay after sweeping to high voltage

# Notes:
# - Sample KSDF: Simple design of two electrodes connected by a drop of DEME-TFSI. 
# - Setup: Voltage applied to one electrode. Leakage current measured at the other.
#          Yokogawa -> Electrode 1 -> DEME -> Electrode 2 -> A/V Amp -> Agilent

use Lab::Moose;
use Lab::Moose::Countdown;
use Time::HiRes qw /time/;
use 5.010;

# Sample
my $sample = 'KSDF';  # chip name -- MoS2 Flake

# Parameters
my $sensitivity = -1e-8; # Sensitivity of A/V-Amp

my $nplc = 1;
my $lineresistance = 150; # resistance of measurement line (Ohm)
my $risetime = 0.100;       # rise time in s
my $live_plot = 1;

my $voltage_list = [3, 0, 2, 0, 1, 0, 0.5];
my $decay_time = 3 * 60;

# temp swee
my $temp_start = 150;
my $temp_stop = 320;
my $temp_step = 10;
my $temp_delay = 5 * 60;

# Instruments ---
my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 22},
);
$multimeter -> sense_nplc(value => $nplc);

my $gate_source = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.01,
    max_units_per_second => 1,
    min_units => -10,
    max_units => 10,
);

$gate_source -> sense_function_on(value => ['CURR']);
$gate_source -> sense_function(value => 'CURR');
$gate_source -> sense_nplc(value => $nplc);

my $temp_control = instrument(
    type => 'Lakeshore350',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 12},
    input_channel => 'A'
);

# Sweep ---
my $time_sweep = sweep(
	type => 'Continuous::Time',
	interval => 0.5,
	duration => $decay_time,
);

# Sweeps with different voltage values 
my $voltage_sweep = sweep(
	type => 'Step::Voltage',
	list => $voltage_list,
	instrument => $gate_source,
);

# Temp sweep
my $temperature_sweep = sweep(
	type => 'Step::Temperature',
	instrument => $temp_control,
	from => $temp_start,
	to => $temp_stop,
	step => $temp_step,
	delay_in_loop => $temp_delay,
	# Stabilizer
    tolerance_setpoint => 1,
    tolerance_std_dev => 0.5,
    measurement_interval => 5,
    observation_time => 20,
);

# Data File ---
my $data_file = sweep_datafile(
    columns => [qw /time duration gate_voltage gate_current sample_current temperature/]
    );

$data_file -> add_plot(
	# x => 'time', 
	# y => 'sample_current',
	curves => [
		{x => 'duration', y => 'sample_current', legend => 'sample_current',
			curve_options => {axes => 'x1y1', linecolor => 'red'}},
		{x => 'duration', y => 'gate_current', legend => 'gate_current', 
			curve_options => {axes => 'x1y1', linecolor => 'blue'}},
		{x => 'duration', y => 'gate_voltage', curve_options => {axes => 'x1y2', linecolor => 'black'}},
	],
	curve_options => {
		with => 'lines',
		linetype => 2,
		linewidth => 2
	},
	plot_options => {
		title => "DEME Freeze - Temperature Sweep",
		format => {x => "'%.2e'", y => "'%.2e'"},
		grid => 1,
		key => 1,
	},
	live => $live_plot,
);

my $time0 = time();

# Measurement ---
my $measurement = sub{
    my $sweep = shift;

    my $amp_sample_voltage = $multimeter -> get_value();
    my $sample_current = $amp_sample_voltage * $sensitivity;

	my $gate_meas = $gate_source -> get_measurement();
	my $gate_current = $gate_meas -> {CURR};

	$sweep -> log(
		gate_voltage => $gate_source -> cached_level(),
		gate_current => $gate_current,
		sample_current => $sample_current,
		temperature => $temp_control -> get_value(channel => 'A'),
		time => time(), 
		duration => time() - $time0,
	);
};

# Run It! ---
$gate_source -> set_level(value => 0);
# start heating
$temp_control -> set_T(value => 1);
$temp_control -> set_heater_range(output => 1, value => 3);

$temperature_sweep -> start(
	slaves => [$voltage_sweep, $time_sweep],
	measurement => $measurement,
	datafile => $data_file,
	folder => $sample.'_deme-decay',
	date_prefix => 1,
);

# Clean-up ---
$gate_source -> set_level(value => 0); # Turn off gate voltage
$temp_control -> set_T(value => 1);
$temp_control -> set_heater_range(output => 1, value => 0);


print('Done!\n');
