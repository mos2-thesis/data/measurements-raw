#!/usr/bin/perl

#--- 0. Import Packages ---#
use Lab::Moose;
use Lab::Moose::Countdown;
use Time::HiRes qw /time/;
use warnings;
use strict;

my $t0 = time();

#--- 1. Set Parameters ---#
my $max_voltage = 3;
my $sample = 'KSTF-A';
my $sensitivity = -1e-8;

my $liveplot = 0;
my $NPLC = 1;
my $id = int(rand(1e6));

#--- 2. Init Instruments ---#

##--- Ionic Liquid ---##
my $ionic = instrument(
  type => 'Keithley2400',
  connection_type => 'LinuxGPIB',
  connection_options => {gpib_address => 29},
  # mandatory protection setting2
  max_units_per_step => 0.0001, # max step is 1mV/1mA
  max_units_per_second => 0.002,
  min_units => -5,
  max_units => 5,
);

$ionic -> source_function(value => 'VOLT');
$ionic -> source_range(value => 3);
$ionic -> set_level(value => 0);
$ionic -> sense_function_on(value => ['CURR']);

##--- Device Bias ---##
my $device_bias = instrument(
  type => 'Yokogawa7651',
  connection_type => 'LinuxGPIB',
  connection_options => {gpib_address => 24},
  max_units_per_step => 0.0001,
  max_units_per_second => 0.001,
  min_units => -10,
  max_units => 10,
);

my $device_multimeter = instrument(
  type => 'Agilent34410A',
  connection_type => 'LinuxGPIB',
  connection_options => {pad => 15}
);

#--- Loop Start ---#
$device_bias -> set_level(value => 0.1);
countdown(2 * 60);

for (my $i = 0.1; $i < $max_voltage + 0.1; $i += 0.1) {

#--- 3. Define Sweeps ---#
my $ionic_sweep = sweep(
  type => 'Step::Voltage',
  instrument => $ionic,
  delay_in_loop => 0,
  delay_before_loop => 0,
  points => [0, $i, -$i, 0],
  step => 0.002
);

#--- 4. Create Datafile and Plot ---#
my $datafile = sweep_datafile(
  filename => $sample.'_training',
  columns => [qw/id time max_gate_voltage gate_voltage gate_current bias_voltage device_current/]
);

$datafile -> add_plot(
  x => 'gate_voltage',
  y => 'device_current',
  plot_options => {
	  title => "DEME-TFSI Training - $sample - $i V",
	  xlabel => 'Gate Voltage [V]',
	  ylabel => 'Device Current [A]',
	  grid => 0
  },
  curve_options => {
	  with => 'lines'
  },
  refresh_interval => 10,
  hard_copy => $sample.'_training.png',
  live => $liveplot
);

#--- 5. Measurement Instructions ---#
my $meas = sub {
  my $sweep = shift;
  my $gate_voltage = $ionic -> cached_level(),
  my $gate_current = $ionic -> get_measurement() -> {CURR};
  my $bias_voltage = $device_bias -> cached_level(),
  my $device_current = ($device_multimeter -> get_value()) * $sensitivity;
  $sweep -> log(
	id => $id,
    time => time(),
	max_gate_voltage => $i,
    gate_voltage => $gate_voltage,
	gate_current => $gate_current,
    bias_voltage => $bias_voltage,
    device_current => $device_current
  );
};

#--- 6. Run Training ---#
$ionic_sweep -> start(
  measurement => $meas,
  datafile => $datafile,
  folder => $id.'_'.$sample.'_training_'.$i,
  date_prefix => 0,
  time_prefix => 0
);

#--- Loop End ---#
};

# Set everything to zero
$device_bias -> set_level(value => 0);
$ionic -> set_level(value => 0);
