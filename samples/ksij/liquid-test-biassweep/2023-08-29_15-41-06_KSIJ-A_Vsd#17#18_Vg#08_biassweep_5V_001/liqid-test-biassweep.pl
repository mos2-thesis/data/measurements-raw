#!/usr/bin/perl
#PODNAME: liquid-test.pl
#ABSTRACT: Measure current when liquid ion gating

use Lab::Moose;
use Lab::Moose::Countdown;
use Time::HiRes qw /time/;
use 5.010;

# Sample

my $sample = 'KSIJ-A_';  # chip name -- MoS2 Flake
my $PINsI = 'Vsd#17#18_';    # pins / cables -- MoS2 flake 
my $PINGate = 'Vg#08_';     # pins / cables -- Liquid Ion Gate

# parameters of the setup

my $lineresistance=150; # resistance of measurement line (Ohm)
my $sensitivity = -1e-7; # sensitivity of DL1211 in A/V
my $risetime = 0.100;       # rise time in s

my $livePlot = 0;
my $NPLC = 1;

# parameters of the bias trace
my $biasstart = -0.01;
my $biasend = 0.01;
my $biasstep = 0.00001;

# instruments
my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 22},
);
#$multimeter->set_nplc(value => $NPLC);

my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.001,
    max_units_per_second => 0.005,
    min_units => -20,
    max_units => 20,
);

$gatesource -> sense_function_on(value => ['CURR']);
$gatesource -> sense_function(value => 'CURR');
$gatesource -> sense_nplc(value => 0.1);

my $biassource = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 1},
    max_units_per_step => 0.005,
    max_units_per_second => 0.005,
    min_units => -0.015,
    max_units => 0.015,
);

# Loop over gatevoltage
my @gatevoltage = (3, 4, 5);

foreach my $value (@gatevoltage) {

# sweep ---
my $biassweep = sweep(
	type => 'Step::Voltage',
	instrument => $biassource,
	from => $biasstart,
	to => $biasend,
	step => $biasstep,
	backsweep => 0,
	delay_before_loop => 0
);

# data file
my $datafile = sweep_datafile(columns => [qw/gate_voltage gate_current sample_current bias_voltage time duration/]);

$datafile -> add_plot(
	curves => [
		{
			x => 'bias_voltage', 
			y => 'gate_current',
			curve_options => {
				with => 'lines',
				linetype => 2,
				linecolor => 'blue',
				linewidth => 2
			},
		},
		{
			x => 'bias_voltage', 
			y => 'sample_current',
			curve_options => {
				with => 'lines',
				linetype => 2,
				linecolor => 'red',
				linewidth => 2
			},
		},
	],
	plot_options => {
		title => "Liquid Ion Gating - $sample$PINsI - $PINGate - Gate $value V",
		format => {x => "'%.2e'", y => "'%.2e'"},
		grid => 0,
	},
	live => 1,
);

# measurement
my $t0 = time();

my $meas = sub {
  my $sweep = shift;
 
  my $ampvoltage = $multimeter -> get_value();
  my $sample_current = $ampvoltage * $sensitivity; 
 
  $sweep -> log(
	gate_voltage => $value,
	gate_current => $gatesource -> get_measurement() -> {CURR},
	sample_current => $sample_current,
	bias_voltage => $biassource -> cached_level(),
	time => time(),
	duration => time() - $t0
  );
};

# run it

	$gatesource -> set_level(value => $value); 
	$biassource -> set_level(value => -0.01);

	countdown(2 * 60);

	$biassweep -> start(
   		measurement => $meas,
    	datafile    => $datafile,
    	folder		=> $sample.$PINsI.$PINGate.'biassweep_'.$value.'V',
    	date_prefix => 1,
	);

	print("Finished loop with $value V!\n");

};

	# go back to zero
	$gatesource -> set_level(value => 0);
	$biassource -> set_level(value => 0);
	
print("Success!\n");



