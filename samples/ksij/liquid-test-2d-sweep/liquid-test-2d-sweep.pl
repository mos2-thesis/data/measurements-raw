#!/usr/bin/perl
#PODNAME: liquid-gate-sweeps.pl
#ABSTRACT: Voltage sweeps at liquid-ion gate and sample electrodes 

use Lab::Moose;
use Lab::Moose::Countdown;
use Time::HiRes qw /time/;
use 5.010;

# Sample Information ---
my $sample = "KSIJ-A";
my $pin_sd = "Vsd#17#18";
my $pin_gate = "Vg#08";

# Parameters ---
my $sensitivity = -1e-7;
my $live_plot = 1;
my $nplc = 10;

# Sweep Parameters ---
my $gate_start = 0;
my $gate_end = 5;
my $gate_step = 0.02;

my $sample_start = -0.01;
my $sample_end = 0.01;
my $sample_step = 0.0001;

# Instruments ---
## Multimeter
my $multimeter = instrument(
	type => "Agilent34410A",
	connection_type => "LinuxGPIB",
	connection_options => {pad => 21}
);

$multimeter -> sense_nplc(value => $nplc);

## Sample Voltage Source
my $sample_source = instrument(
	type => "YokogawaGS200",
	connection_type => "LinuxGPIB",
	connection_options => {pad => 1},
	max_units_per_step => 0.005,
	max_units_per_second => 0.005,
	min_units => -0.015,
	max_units => 0.015
);

## Gate Voltage Source
my $gate_source = instrument(
	type => "Keithley2400",
	connection_type => "LinuxGPIB",
	connection_options => {pad => 29},
	max_units_per_step => 0.001,
	max_units_per_second => 0.005,
	min_units => -6,
	max_units => 6 
);

$gate_source -> sense_function_on(value => ["CURR"]);
$gate_source -> sense_function(value => "CURR");
$gate_source -> sense_nplc(value => $nplc);

# Data ---
## Data File
my $data_file = sweep_datafile(
	columns => [qw /gate_voltage gate_current sample_voltage sample_current time/]
);

## Plots
$data_file -> add_plot(
	type => 'pm3d',
	x => "gate_voltage",
	y => "sample_voltage",
	z => "gate_current",
	plot_options => {
		title => "Liquid-Ion Sweep",
		xlabel => "Gate Voltage [V]",
		ylabel => "Sample Voltage [V]",
		cblabel => "Gate Current [A]",
		grid => 0
	},
	hard_copy => 'gate_current.png',
	live => $live_plot
);

$data_file -> add_plot(
	type => 'pm3d',
	x => "gate_voltage",
	y => "sample_voltage",
	z => "gate_current",
	plot_options => {
		title => "Liquid-Ion Sweep",
		xlabel => "Gate Voltage [V]",
		ylabel => "Sample Voltage [V]",
		cblabel => "Sample Current [A]",
		grid => 0
	},
	hard_copy => 'sample_current.png',
	live => $live_plot
);

# Sweeps ---
## Gate Sweep
my $gate_sweep = sweep(
	type => "Step::Voltage",
	instrument => $gate_source,
	from => $gate_start,
	to => $gate_end,
	step => $gate_step
);

## Sample Sweep
my $sample_sweep = sweep(
	type => "Step::Voltage",
	instrument => $sample_source,
	delay_in_loop => 0.05,
	delay_before_loop => 10,
	from => $sample_start,
	to => $sample_end,
	step => $sample_step
);

# Measurement ---
my $measurement = sub
{
	my $sweep = shift;

	my $gate_voltage = $gate_source -> cached_level();
	my $gate_current = $gate_source -> get_measurement() -> {CURR};
	my $sample_voltage = $sample_source -> cached_level();
	my $sample_current = $multimeter -> get_value() * $sensitivity;
	
	$sweep -> log(
		gate_voltage => $gate_voltage,
		sample_voltage => $sample_voltage,
		gate_current => $gate_current,
		sample_current => $sample_current,
		time => time()
	)
};

# Run It! ---
$gate_source -> set_level(value => 0);
$sample_source -> set_level(value => -0.01);

$gate_sweep -> start(
	slave => $sample_sweep,
	measurement => $measurement,
	datafile => $data_file,
	folder => $sample."-".$pin_sd."-"."vg-vsd-2d-sweep",
	date_prefix => 1
);

# Aftercare ---
$gate_source -> set_level(value => 0);
$sample_source -> set_level(value => 0);

print("Success!\n");
