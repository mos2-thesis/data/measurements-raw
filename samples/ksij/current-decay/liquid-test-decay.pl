#!/usr/bin/perl
#PODNAME: liquid-test-decay.pl
#ABSTRACT: Measure the decay of leak current

use Lab::Moose;
use Lab::Moose::Countdown;
use Time::HiRes qw /time/;
use 5.010;

# Sample Information ---
my $sample = "KSIJ-A";
my $pin_sd = "Vsd#17#18";
my $pin_gate = "Vg#08";

# Parameters ---
my $sensitivity = -1e-6;
my $live_plot = 1;
my $nplc = 10;

my $gate_voltage = 4;
my $sample_voltage = 0;
my $wait_time = 120 * 60;

# Instruments ---
## Multimeter
my $multimeter = instrument(
	type => "Agilent34410A",
	connection_type => "LinuxGPIB",
	connection_options => {pad => 21}
);

$multimeter -> sense_nplc(value => $nplc);

## Sample Voltage Source
my $sample_source = instrument(
	type => "YokogawaGS200",
	connection_type => "LinuxGPIB",
	connection_options => {pad => 1},
	max_units_per_step => 0.005,
	max_units_per_second => 0.005,
	min_units => -0.015,
	max_units => 0.015
);

## Gate Voltage Source
my $gate_source = instrument(
	type => "Keithley2400",
	connection_type => "LinuxGPIB",
	connection_options => {pad => 29},
	max_units_per_step => 0.01,
	max_units_per_second => 0.1,
	min_units => -6,
	max_units => 6 
);

$gate_source -> sense_function_on(value => ["CURR"]);
$gate_source -> sense_function(value => "CURR");
$gate_source -> sense_nplc(value => $nplc);

# Data ---
## Data File
my $data_file = sweep_datafile(
	columns => [qw /gate_voltage gate_current sample_voltage sample_current time minutes/]
);

## Plots
$data_file -> add_plot(
	curves => [
		{
			x => "minutes",
			y => "gate_current",
			curve_options => {
				with => "lines",
				linewidth => 2,
				linetype => 2,
				legend => "Gate Current [A]"
			}
		},
		{
			x => "minutes",
			y => "sample_current",
			curve_options => {
				with => "lines",
				linewidth => 2,
				linetype => 2,
				legend => "Sample Current [A]"
			}
		}
	],
	plot_options => {
		title => "Liquid-Ion Current Decay",
		xlabel => "Time in Minutes",
		ylabel => "Gate Current [A]",
	},
	live => $live_plot
);

# Time Sweep ---
my $time_sweep = sweep(
	type => "Continuous::Time",
	interval => 0,
	duration => $wait_time
);

# Measurement ---

$gate_source -> set_level(value => $gate_voltage);
$sample_source -> set_level(value => $sample_voltage);
countdown(30 * 60);
$gate_source -> set_level(value => 0);
$sample_source -> set_level(value => 0);

my $t0 = time();

my $measurement = sub
{
	my $sweep = shift;

	my $gate_voltage = $gate_source -> cached_level();
	my $gate_current = $gate_source -> get_measurement() -> {CURR};
	my $sample_voltage = $sample_source -> cached_level();
	my $sample_current = $multimeter -> get_value() * $sensitivity;
	
	$sweep -> log(
		gate_voltage => $gate_voltage,
		sample_voltage => $sample_voltage,
		gate_current => $gate_current,
		sample_current => $sample_current,
		time => time(),
		minutes => (time() - $t0) / 60
	)
};

# Run It! ---
$gate_source -> set_level(value => 0);
$sample_source -> set_level(value => -0.01);

$time_sweep -> start(
	measurement => $measurement,
	datafile => $data_file,
	folder => $sample."_".$pin_sd."_".$pin_gate."_"."current-decay",
	date_prefix => 1
);

# Aftercare ---
$gate_source -> set_level(value => 0);
$sample_source -> set_level(value => 0);

print("Success!\n");
