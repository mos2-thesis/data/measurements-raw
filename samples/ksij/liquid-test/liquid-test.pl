#!/usr/bin/perl
#PODNAME: liquid-test.pl
#ABSTRACT: Measure current when liquid ion gating

use Lab::Moose;
use Lab::Moose::Countdown;
use Time::HiRes qw /time/;
use 5.010;

# Sample

my $sample = 'KSIJ-A_';  # chip name -- MoS2 Flake
my $PINsI = 'Vsd#18_';    # pins / cables -- MoS2 flake 
my $PINGate = 'Vg#08_';     # pins / cables -- Liquid Ion Gate

# parameters of the setup

my $lineresistance=150; # resistance of measurement line (Ohm)
my $sensitivity = -1e-7; # sensitivity of DL1211 in A/V
my $risetime = 0.100;       # rise time in s

my $livePlot = 0;
my $NPLC = 1;

# parameters of the gate trace
my $gatestart = 0;
my $gateend = 4;
my $gatestep = 0.25;
my $gateDelay = 0; # s
# my $gateInDelay = 0.5 * $risetime; # s
my $gateInDelay = 0; # s
my $gatebackswp = 0; # Do a backsweep (1) or not (0)

# instruments
my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 22},
);
#$multimeter->set_nplc(value => $NPLC);

my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.01,
    max_units_per_second => 0.25,
    min_units => -20,
    max_units => 20,
);
$gatesource -> sense_function_on(value => ['CURR']);
$gatesource -> sense_function(value => 'CURR');
$gatesource -> sense_nplc(value => 0.1);

# sweeps ---
## gatesweeps
my $gatesweep_1 = sweep(
    type       => 'Step::Voltage',
    instrument => $gatesource,
    from => $gatestart, to => $gateend, step => $gatestep,
    backsweep => $gatebackswp,
	delay_before_loop => $gateDelay,
);

my $gatesweep_2 = sweep(
    type       => 'Step::Voltage',
    instrument => $gatesource,
    from => $gateend, to => $gatestart, step => $gatestep,
    backsweep => $gatebackswp,
	delay_before_loop => $gateDelay,
);

## timesweep
my $timesweep_1 = sweep(
	type => 'Continuous::Time',
	interval => 0,
	duration => 1 * 60
);

my $timesweep_2 = sweep(
	type => 'Continuous::Time',
	interval => 0,
	duration => 5 * 60
);

my $timesweep_3 = sweep(
	type => 'Continuous::Time',
	interval => 0,
	duration => 10 * 60
);

# data file
my $datafile = sweep_datafile(columns => [qw/gate_voltage gate_current sample_current time duration/]);

# plot -- no plot. pc keeps freezing. gnuplot not working properly.
# $datafile -> add_plot(
# 	x => 'duration',
#	y => 'gate_current',
#	plot_options => {
#		title => "Liquid Ion Gating - $sample$PINsI - $PINGate - Gate $gateend V",
#		format => {x => "'%.2f s'", y => "'%.2e'"},
#		grid => 0,
#	},
#	curve_options => {
#		with => 'lines',
#		linetype => 2,
#		linecolor => 'black',
#		linewidth => 2,
#	},
#	live => $livePlot,
#);

# measurement
my $t0 = time();

my $meas = sub {
  my $sweep = shift;
 
  my $ampvoltage = $multimeter -> get_value();
  my $sample_current = $ampvoltage * $sensitivity; 
 
  $sweep -> log(
	gate_voltage => $gatesource -> cached_level(),
	gate_current => $gatesource -> get_measurement() -> {CURR},
	sample_current => $multimeter -> get_value(),
	time => time(),
	duration => time() - $t0
  );
};

# run it
$gatesource -> set_level(value => 0); # better would be to throw an error
$timesweep_1 -> start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> '01_'.$sample.$PINsI.$PINGate.'timesweep_01_'.$gateend.'V',
    date_prefix => 1,
);
$gatesweep_1 -> start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> '02_'.$sample.$PINsI.$PINGate.'gatesweep_01_'.$gateend.'V',
    date_prefix => 1,
);
$timesweep_2 -> start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> '03_'.$sample.$PINsI.$PINGate.'timesweep_02_'.$gateend.'V',
    date_prefix => 1,
);
$gatesweep_2 -> start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> '04_'.$sample.$PINsI.$PINGate.'gatesweep_02_'.$gateend.'V',
    date_prefix => 1,
);
$timesweep_3 -> start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> '05_'.$sample.$PINsI.$PINGate.'timesweep_03_'.$gateend.'V',
    date_prefix => 1,
);

# go back to zero, if backsweep is disabled
$gatesource -> set_level(value => 0);

print("Success!\n");
