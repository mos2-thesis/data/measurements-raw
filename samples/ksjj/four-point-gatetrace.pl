use 5.36.0;
use Lab::Measurement;


#Sample description
my $sample = 'KSIJ-A; '; #chip name

#set parameters at devices of the setup

my $sensitivity = -1e-8; #sensitivity of Ithaco in A/V, sign comes from device
my $risetime = 30; #risetime in ms

my $biasvoltage = 1; #voltage set to biasyokogawa

my $gate = Instrument('YokogawaGS200',
    {
    connection_type => 'LinuxGPIB',
    gpib_address => 1,
    gate_protect => 0
    });

my $bias = Instrument('Yokogawa7651',
    {
    connection_type => 'LinuxGPIB',
    gpib_address => 33,
    gate_protect => 0
    });


my $multimeter_current = Instrument('Agilent34410A',
    {
    connection_type => 'LinuxGPIB',
    gpib_address => 22,
    nplc => $NPLC                  # integration time in number of powerline cylces [(1/50)]
    });
 
 #----------------------------------------------------------
 
 my $voltage_sweep = Sweep('Voltage', {
        instrument => $gate,
        mode => 'step',
        points => [0, 1],
        rate => [0.02, 0.02],
        stepwidth => [0.001],
        jump => 1,
        backsweep => 0,
        delay_before_loop => 5,

});
#-------------------------------------------------------

my $file = my_DataFile('Gatetrace '.$sample);

sub my_DataFile {
my $filename = shift;

my $DataFile = DataFile($filename);

$DataFile->add_column('GATE');
$DataFile->add_column('I_DC');
$DataFile->add_column('BIAS');
$DataFile->add_column('COND');
$DataFile->add_column('R_DC');

my $plot = {
                'autosave' => 'last', # last, allways, never
                'title' => 'Gatetrace of' .$sample.$PINsI,
                'type' => 'linetrace', #'lines', #'linetrace', #point
                'x-axis' => 'GATE',
                #'x-min' => -1,
                #'x-max' => 1,
                'y-axis' => 'I_DC',
                'y-format' => '%1.2e',
                #'y-min' => -1,
                #'y-max' => 1,
                'grid' => 'xtics ytics',
                };
$DataFile -> add_plot($plot);

return $DataFile;

}

#-------------------------------------------------------------

my $measurement = sub {

        my $sweep = shift;

        my $current = $multimeter_current->get_value();
        my $gatevoltage = $gate -> get_value({read_mode => 'cache'});

        my $i_dc = $current*($sensitivity); 
        my $v_g = $gatevoltage;
        my $g_dc = ($i_dc)/($samplebias)*25812.807;
        my $r_dc = ($i_dc != 0) ? ($samplebias)/($i_dc) : '?';

        $sweep->LOG({
                GATE => $v_g,
                I_DC => $i_dc,
                BIAS => $samplebias,
                COND => $g_dc,
                R_DC => $r_dc,
        });

};

#-----------------------------------------------------------------------------

$file -> add_measurement($measurement);

$voltage_sweep -> add_DataFile($file);


$voltage_sweep -> start();
