#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use Time::HiRes qw /time/;
use Lab::Moose::Countdown;
use Lab::Moose::Stabilizer;
use 5.010;

# Sample
my $sample = 'KSJJ_';  # chip name
my $PINsI = 'Vsd#18#16_V#18#17_';    # pins / cables
my $PINGate = 'Vg#9_';     # pins / cables

# parameters of the setup
my $lineresistance=150; # resistance of measurement line (Ohm)
my $sensitivity = -1e-8; # sensitivity of DL1211 in A/V 
my $risetime = 0.100; # rise time in s

my $biasvoltage = 0.01; # voltage set to biasyokogawa
my $divider = 1; # voltage divider

my $samplebias = ($biasvoltage/$divider);

my $livePlot = 1;
my $NPLC = 1;

my $temp_start = 220;
my $temp_end = 0;
my $temp_step = 1;

my $gate_voltage = 2;

# voltageprobe
my $voltsense = 1;
my $voltageDB = 0;
my $voltageCutoff = 1e3;

# instruments
my $currentprobe = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 11},
);
#$multimeter->set_nplc(value => $NPLC);

my $voltprobe = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 15},
);
#$multimeter->set_nplc(value => $NPLC);

# biassource
my $biassource = instrument(
    type => 'Yokogawa7651',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 3},
    max_units_per_step => 1,
    max_units_per_second => 10,
    min_units => -20,
    max_units => 20,
);

# temperature control
my $temp_control = instrument(
    type => 'Lakeshore340',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 12},
    input_channel => 'A'
);

 my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.005,
    max_units_per_second => 0.1,
    min_units => -20,
    max_units => 20,
);
$gatesource -> sense_function_on(value => ['CURR']);
$gatesource -> sense_nplc(value => 0.1);
 
# sweep
my $time_sweep = sweep(
    type => 'Continuous::Time',
	interval => 2,
	duration => 2 * 60 * 60
);

# my $temp_sweep_step = sweep(
# 	type => 'Step::Temperature',
# 	instrument => $temp_control,
# 	from => $temp_start,
# 	to => $temp_end,
# 	step => $temp_step,
# 	tolerance_setpoint => 0.01,
# 	tolerance_std_dev => 0.01,
# 	measurement_interval => 1,
# 	observation_time => 20,
# 	verbose => 1
# );
# 
# my $temp_sweep_continuous = sweep(
# 	type => 'Continuous',
# 	instrument => $temp_control,
# 	points => [220, 180, 5],
# 	rates => [5, 3, 1],
# 	intervals => [1]
# );
 
# data file
my $datafile = sweep_datafile(columns => [qw/gate_voltage gate_current bias_voltage bias_current probe_voltage temperature time/]);

# plot 
$datafile -> add_plot(
	x => 'temperature',
	y => 'bias_current',
	plot_options => {
		title => "Cooldown Temp Sweep - $sample$PINsI - $PINGate - V_Bias = $samplebias V",
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
	curve_options => {
		with => 'lines',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	},
	refresh_interval => 10,
	live => $livePlot,
);

# measurement
my $measurement = sub {
  my $sweep = shift;
 
  my $bias_current = ( $currentprobe -> get_value() ) * $sensitivity; 
  my $probe_voltage = ( $voltprobe -> get_value() ) * $voltsense;
  my $gate_current = $gatesource -> get_measurement() -> {CURR};
 
  $sweep->log(
	  gate_voltage => $gate_voltage, # constant
	  gate_current => $gate_current,
	  bias_voltage => $samplebias, # constant
	  bias_current => $bias_current,
	  probe_voltage => $probe_voltage,
	  temperature => $temp_control -> get_value(channel => 'A'),
	  time => time()
  );
};

# run it
# stabilize(
# 	instrument => $temp_control,
# 	setpoint => $temp_start,
# 	getter => 'get_T',
# 	tolerance_setpoint => 0.05,
# 	tolerance_std_dev => 0.05,
# 	measurement_interval => 1,
# 	observation_time => 60,
# 	verbose => 1
#);

$biassource->set_level(value => $biasvoltage);
$gatesource -> set_level(value => $gate_voltage);
$temp_control -> set_heater_range(value => 0);
$time_sweep -> start(
    measurement => $measurement,
    datafile    => $datafile,
    folder		=> $sample.$PINsI.$PINGate.'3point-timesweep',
    date_prefix => 1,
);

# go back to zero, if backsweep is disabled
$biassource -> set_level(value => 0);

