#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use Time::HiRes qw /time/;
use 5.010;

# Sample

my $sample = 'RSMoS2ionic01_';  # chip name
my $PINsI = 'Vsd#5#18_';    # pins / cables
my $PINGate = 'Vg#9_';     # pins / cables

# parameters of the setup
my $lineresistance=150; # resistance of measurement line (Ohm)
my $sensitivity = -1e-7; # sensitivity of DL1211 in A/V
my $risetime = 0.100;       # rise time in s

my $biasvoltage = 1;   # voltage set to biasyokogawa
my $divider = 1000;      # voltage divider

my $samplebias = ($biasvoltage/$divider);

my $livePlot = 1;
my $NPLC = 1;

# voltageprobe
my $voltsense = 0.1;
my $voltageDB = 20;
my $voltageCutoff = 1e3;

# parameters of the gate trace
my $gatevalue = 1.0;

# instruments
my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 11},
);
#$multimeter->set_nplc(value => $NPLC);

my $voltprobe = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 15},
);
#$multimeter->set_nplc(value => $NPLC);

my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.005,
    max_units_per_second => 0.1,
    min_units => -20,
    max_units => 20,
);
$gatesource->sense_function_on(value => ['CURR']);
$gatesource->sense_nplc(value => 0.1);
$gatesource->set_level(value => $gatevalue);

# biassource
my $biassource = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 1},
    max_units_per_step => 1,
    max_units_per_second => 10,
    min_units => -20,
    max_units => 20,
);

my $lakeshore = instrument(
    type => 'Lakeshore340',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 12},
    input_channel => 'A', # set default input channel for all method calls
);

# sweep
my $sweep = sweep(
    type       => 'Continuous::Time',
	interval  => 5,
	duration => 12 * 60 * 60,
);

# data file
my $datafile = sweep_datafile(columns => [qw/gate_voltage gate_current bias_current bias_voltage sample_voltage time temperature/]);

# plot 
$datafile->add_plot(
	x => 'temperature',
	y => 'bias_current',
	plot_options => {
		title => "Gatesweep - $sample$PINsI - $PINGate - V_Bias = $samplebias V - Vgate = $gatevalue",
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
	curve_options => {
		with => 'lines',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	},
	refresh_interval => 10,
	live => $livePlot,
);

# measurement
my $meas = sub {
  my $sweep = shift;
 
  my $ampvoltage = $multimeter -> get_value();
  my $i_dc = $ampvoltage*$sensitivity; 
  my $sample_voltage = ( $voltprobe -> get_value() ) * $voltsense;
  my $gate_current = $gatesource->get_measurement()->{CURR};
  my $temperature = $lakeshore->get_value(channel => 'A');
 
  $sweep->log(
	  gate_voltage => $gatesource -> cached_level(),
	  bias_current => $i_dc,
	  gate_current => $gate_current,
	  sample_voltage => $sample_voltage,
	  bias_voltage => $biasvoltage,
	  temperature => $temperature,
	  time => time()
  );
};

# run it
$biassource->set_level(value => $biasvoltage);
$sweep->start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> $sample.$PINsI.$PINGate.'gatetrace',
    date_prefix => 1,
);

# go back to zero, if backsweep is disabled
# $gatesource->set_level(value => 0);
# $biassource->set_level(value => 0);
