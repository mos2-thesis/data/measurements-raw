#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use 5.010;

# Sample

my $sample = 'RSEF_';  # chip name
my $PINsI = 'Vsd#7#8_';    # pins / cables
my $PINGate = 'Vg#16_';     # pins / cables

# parameters of the setup

my $lineresistance=220; # resistance of measurement line (Ohm)
my $sensitivity = 1e-9; # sensitivity of DL1211 in A/V
my $risetime = 0 * 100e-3;       # rise time in ms

my $biasstart = -1.2;
my $biasstop = 1;
my $biasstep = 0.001;
my $divider = 100;      # voltage divider

my $bias_init_wait = 5;

# parameters of the gate trace
my $gatevoltage = 0;

# instruments

my $multimeter = instrument(
    type => 'HP3458A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 15},
);
$multimeter->set_nplc(value => '2');

my $biasyoko = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 1},
    max_units_per_step => 0.25,
    max_units_per_second => 1.0,
    min_units => -5,
    max_units => 5,
);

my $gateyoko = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.01,
    max_units_per_second => 0.2,
    min_units => -120,
    max_units => 120,
);

# instruments
my $lakeshore = instrument(
    type => 'Lakeshore340',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 19},
    input_channel => 'A', # set default input channel for all method calls
);
my $temperature = $lakeshore->get_sensor_units_reading();

# sweep
my $sweep = sweep(
	type       => 'Step::Voltage',
	instrument => $biasyoko,
	#    delay_in_loop => 0.05,
	from => $biasstart, to => $biasstop, step => $biasstep,
	backsweep => 0,
	delay_before_loop => $bias_init_wait,
	delay_in_loop => $risetime,
);

# data file

my $datafile = sweep_datafile(columns => [qw/voltage current/]);

# plot 
$datafile->add_plot(
	x => 'voltage',
	y => 'current',
	plot_options => {
		title => "Biassweep T=$temperature Ohm - $sample$PINsI$PINGate - V_Gate = $gatevoltage V",
		format => {x => "'%.1e'", y => "'%.3e'"},
		grid => 0,
	},
	curve_options => {
		with => 'lines',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	}
);

# measurement

my $meas = sub {
  my $sweep = shift;
 
  my $ampvoltage = $multimeter->get_value();
  my $i_dc = $ampvoltage*$sensitivity; 
 
  $sweep->log(
        voltage => $biasyoko->cached_level()/$divider,
        current => $i_dc,
  );
};

# run it

$gateyoko->set_level(value => $gatevoltage);
$sweep->start(
    measurement => $meas,
    datafile    => $datafile,
    folder 		=> $sample.$PINsI.$PINGate.'biastrace',
    date_prefix => 1,
);
