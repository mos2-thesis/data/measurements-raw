#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use Time::HiRes qw /time/;
use 5.010;

# Sample

my $sample = 'KSJJ_';  # chip name
my $PINsI = 'Vsd#5#18_';    # pins / cables
my $PINGate = 'Vg#9_';     # pins / cables

# parameters of the setup

my $lineresistance = 150; # resistance of measurement line (Ohm)
my $sensitivity = -1e-9; # sensitivity of DL1211 in A/V
my $risetime = 0.100;       # rise time in s

my $biasvoltage = 10;   # voltage set to biasyokogawa
my $divider = 1000;      # voltage divider

my $samplebias = ($biasvoltage/$divider);

my $livePlot = 1;
my $NPLC = 1;

# approx temperature (high noise with lakeshore)
my $temp = 'unkown';

# voltageprobe
my $voltsense = 0.01;
my $voltageDB = 20;
my $voltageCutoff = 1e3;

# parameters of the gate trace
my $gatestart = 1.0;
my $gateend = 3;
my $gatestep = 0.005;
my $gateDelay = 0; # s
# my $gateInDelay = 0.5 * $risetime; # s
my $gateInDelay = 0; # s
my $gatebackswp = 0; # Do a backsweep (1) or not (0)

# parameters of temp sweep
my $temp_start = 230; # K
my $temp_end = 180; # K
my $temp_step = 1; # K

# instruments
my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 11},
);
#$multimeter->set_nplc(value => $NPLC);

my $voltprobe = instrument(
    type => 'HP34420A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 17},
);
#$multimeter->set_nplc(value => $NPLC);

my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.005,
    max_units_per_second => 0.1,
    min_units => -20,
    max_units => 20,
);
$gatesource -> sense_function_on(value => ['CURR']);
$gatesource -> sense_nplc(value => 0.1);

# biassource
my $biassource = instrument(
    type => 'Yokogawa7651',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 1},
    max_units_per_step => 1,
    max_units_per_second => 10,
    min_units => -20,
    max_units => 20,
);

# temperature control
my $temp_control = instrument(
    type => 'Lakeshore340',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 12},
    input_channel => 'A'
);

# sweep
my $temp_sweep = sweep(
    type => 'Step::Temperature',
    instrument => $temp_control,
    from => $temp_start, 
    to => $temp_end,
    step => $temp_step,
    delay_before_loop => 30 * 60,
    delay_in_loop => 2 * 60,
    
    # Stabilizer Options 
    tolerance_setpoint => 0.01,
    tolerance_std_dev => 0.01,
    measurement_interval => 1,
    observation_time => 30,
    verbose => 1
    );

my $sweep = sweep(
    type       => 'Step::Voltage',
    instrument => $gatesource,
    from => $gatestart, to => $gateend, step => $gatestep,
    backsweep => $gatebackswp,
	delay_before_loop => $gateDelay,
);

# data file
my $datafile = sweep_datafile(columns => [qw/gate_voltage bias_current gate_current bias_voltage sample_voltage time/]);

# plot 
$datafile->add_plot(
	x => 'temperature',
	y => 'probe_voltage',
	plot_options => {
		title => "Probe Voltage - $sample$PINsI - $PINGate - V_Bias = $samplebias V - T = $temp",
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
	curve_options => {
		with => 'lines',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	},
	refresh_interval => 10,
	live => $livePlot,
);

# measurement
my $meas = sub {
  my $sweep = shift;
 
  my $ampvoltage = $multimeter -> get_value();
  my $i_dc = $ampvoltage * $sensitivity; 
  my $probe_voltage = ( $voltprobe -> get_value() ) * $voltsense;
  my $gate_current = $gatesource -> get_measurement() -> {CURR};
  my $temperature = 
 
  $sweep->log(
	  gate_voltage => $gatesource -> cached_level(),
	  gate_current => $gate_current,
	  bias_voltage => $biasvoltage,
	  bias_current => $i_dc,
	  probe_voltage => $probe_voltage,
	  temperature => $temp_control -> get_value(channel => 'A'),
	  time => time()
  );
};

# run it
$biassource->set_level(value => $biasvoltage);
$sweep->start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> $sample.$PINsI.$PINGate.'gatetrace',
    date_prefix => 1,
);

# go back to zero, if backsweep is disabled
$gatesource->set_level(value => 0);
$biassource->set_level(value => 0);
