#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use Time::HiRes qw /time/;
use Lab::Moose::Countdown;
use 5.010;

# Sample

my $sample = 'KSJJ_';  # chip name
my $PINsI = 'Vsd#18#16_V#18#17_';    # pins / cables
my $PINGate = 'Vg#9_';     # pins / cables

# parameters of the setup

my $lineresistance=150; # resistance of measurement line (Ohm)
my $sensitivity = -1e-8; # sensitivity of DL1211 in A/V 
my $risetime = 0.100;       # rise time in s

my $biasvoltage = 0.05;   # voltage set to biasyokogawa
my $divider = 100;      # voltage divider

my $samplebias = ($biasvoltage/$divider);

my $livePlot = 1;
my $NPLC = 1;

# approx temperature (high noise with lakeshore)
my $temperature = 222;

# voltageprobe
my $voltsense = 1;
my $voltageDB = 0;
my $voltageCutoff = 1e3;

# parameters of the gate trace
my $gatestart = 0.0;
my $gateend = 0;
my $gatestep = 0.005;
my $gateDelay = 0; # s
# my $gateInDelay = 0.5 * $risetime; # s
my $gateInDelay = 0; # s
my $gatebackswp = 0; # Do a backsweep (1) or not (0)

# instruments
my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 11},
);
#$multimeter->set_nplc(value => $NPLC);

my $voltprobe = instrument(
    type => 'HP34420A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 17},
);
#$multimeter->set_nplc(value => $NPLC);

my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.005,
    max_units_per_second => 0.1,
    min_units => -20,
    max_units => 20,
);
$gatesource -> sense_function_on(value => ['CURR']);
$gatesource -> sense_nplc(value => 0.1);

# biassource
my $biassource = instrument(
    type => 'Yokogawa7651',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 3},
    max_units_per_step => 1,
    max_units_per_second => 10,
    min_units => -20,
    max_units => 20,
);

# temperature control
my $temp_control = instrument(
    type => 'Lakeshore340',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 12},
    input_channel => 'A'
);


# sweep
my $sweep = sweep(
    type       => 'Step::Voltage',
    instrument => $gatesource,
	# from => $gatestart, to => $gateend, step => $gatestep,
	points => [0, -4, 3, 0], steps => [$gatestep, $gatestep, $gatestep],
    backsweep => $gatebackswp,
	delay_before_loop => $gateDelay,
);

# data file
my $datafile = sweep_datafile(columns => [qw/gate_voltage bias_current gate_current bias_voltage probe_voltage temperature time/]);

# plot 
$datafile->add_plot(
	x => 'gate_voltage',
	y => 'bias_current',
	plot_options => {
		title => "Gatesweep - $sample$PINsI - $PINGate - V_Bias = $samplebias V - T ~ $temperature K",
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
	curve_options => {
		with => 'lines',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	},
	refresh_interval => 10,
	live => $livePlot,
);

# measurement
my $meas = sub {
  my $sweep = shift;
 
  my $bias_current = ( $multimeter -> get_value() ) * $sensitivity; 
  my $probe_voltage = ( $voltprobe -> get_value() ) * $voltsense;
  my $gate_current = $gatesource -> get_measurement()->{CURR};
 
  $sweep->log(
	  gate_voltage => $gatesource -> cached_level(),
	  gate_current => $gate_current,
	  bias_voltage => $biasvoltage,
	  bias_current => $bias_current,
	  probe_voltage => $probe_voltage,
	  temperature => $temperature,
	  time => time()
  );
};

# run it
$biassource->set_level(value => $biasvoltage);
$sweep->start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> $sample.$PINsI.$PINGate.'gatetrace',
    date_prefix => 1,
);

# go back to zero, if backsweep is disabled
$gatesource -> set_level(value => 0);
$biassource -> set_level(value => 0);
