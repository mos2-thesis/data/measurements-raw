#!/usr/bin/perl
#PODNAME: coulomb-diamonds.pl
#ABSTRACT: Measure a quantum dot stability diagram, i.e. current versus gate and bias

use Lab::Moose;
use 5.010;


# sample description
my $sample 	= 'RSEF_';  # chip name
my $PINsI 	= 'Vsd#7#8_';    # pins / cables
my $PINGate = 'Vg#16_';     # pins / cables

my $live_plot = 0; # change if plots shall pop up or not

# parameters of the setup
my $lineresistance=220;   # resistance of measurement line (Ohm)

# parameters of the measurement

my $biasstart 	= -0.3;    # start value of bias sweep in VOLTS
my $biasend 	=  0.3;       # end value of bias sweep in VOLTS
# my $stepwidthVb =  0.006; # stepwidth of the biasyoko in VOLTS
my $biassteps = 50;
my $stepwidthVb =  abs($biasstart - $biasend)/$biassteps; # stepwidth of the biasyoko in VOLTS

my $divider 	= 100;      # voltage divider between biasyoko and sample

#my $gateoffset = -0;     # gate offset value in VOLTS
my $gatestart 	= 21.0;       # start value of gate sweep in VOLTS
my $gateend 	= 10;         # end value of gate sweep in VOLTS
my $stepwidthVg = 0.002; # stepwidth of the gateyoko in VOLTS
my $gateWait 	= 0 * 60;

my $sensitivity = -1e-9;# sensitivity of DL1211 in A/V, sign comes from device
my $risetime 	= 0.5 * 100e-3;       # rise time in ms

my $NPLC		= 1;            # DMM integration time in 1/50 sec
my $TEMP = 4.2; # K

# instruments
my $multimeter = instrument(
    type => 'HP3458A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 15},
);
$multimeter->set_nplc(value => $NPLC);

# instruments
my $lakeshore = instrument(
    type => 'Lakeshore340',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 19},
     
    input_channel => 'A', # set default input channel for all method calls
);

my $biasyoko = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 1},
    max_units_per_step => 0.05,
    max_units_per_second => 1.0,
    min_units => -5,
    max_units => 5,
);

my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.01,
    max_units_per_second => 0.2,
    min_units => -80,
    max_units => 80,
);
$gatesource->sense_function_on(value => ['CURR']);
$gatesource->sense_nplc(value => 2);
my $gatecurrent = $gatesource->get_measurement()->{CURR};

my $before_biassweep = sub {
    # $TEMP = $lakeshore->get_sensor_units_reading();
	$gatecurrent = $gatesource->get_measurement()->{CURR};
};


# Sweeps
my $gate_sweep = sweep(
    type       => 'Step::Voltage',
    instrument => $gatesource,
    from => $gatestart, to => $gateend, step => $stepwidthVg,
	delay_before_loop => $gateWait,
);
 
my $bias_sweep = sweep(
    type       => 'Step::Voltage',
    instrument => $biasyoko,
    from => $biasstart, to => $biasend, step => $stepwidthVb,
    before_loop => $before_biassweep,
	delay_in_loop => $risetime,
);

# Data file
my $datafile = sweep_datafile(columns => [qw/gate gatecurrent bias current temperature/]);
$datafile->add_plot(
    type    => 'pm3d',
    x       => 'gate',
    y       => 'bias',
    z       => 'current',
	refresh => 'block',
	live => $live_plot,
	plot_options => {
		title => "CBs - $sample$PINsI - $PINGate - Tstart = $TEMP Ohm",
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
);
#gatecurrent plot
$datafile->add_plot(
	x => 'gate',
	y => 'gatecurrent',
	refresh => 'block',
	live => $live_plot,
	plot_options => {
		title => "Diamonds-Gatecurr - $sample$PINsI$PINGate - T = $TEMP Ohm",
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
	curve_options => {
		with => 'points',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	},
	hard_copy_suffix => '_gatecurrent',
);

# Measurement
my $meas = sub {
    my $sweep = shift;
    my $current = ($multimeter->get_value()*($sensitivity));
    my $v_b = (($biasyoko->cached_level())/($divider)); 
    $sweep->log(
        gate    => $gatesource->cached_level(),
		gatecurrent => $gatecurrent,
        bias    => $v_b,
        current => $current,
        temperature => $TEMP,
    );
};

# Run it all

$gate_sweep->start(
    slave       => $bias_sweep,
    measurement => $meas,
    datafile    => $datafile,
    folder      => $sample.$PINsI.$PINGate.'diamonds',
    date_prefix => 1,
);
