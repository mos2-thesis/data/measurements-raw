#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use 5.010;

my $lakeshore = instrument(
    type => 'Lakeshore340',
    connection_type => 'VISA::GPIB',
    connection_options => {pad => 19},
     
    input_channel => 'A', # set default input channel for all method calls
);
say $lakeshore->get_sensor_units_reading(channel => 'A');
say $lakeshore->get_sensor_units_reading();

my $biasyoko = instrument(
    type => 'YokogawaGS200',
    connection_type => 'VISA::GPIB',
    connection_options => {pad => 1},
    max_units_per_step => 0.005,
    max_units_per_second => 0.01,
    min_units => -0.2,
    max_units => 0.2,
);

my $gateyoko = instrument(
    type => 'Keithley2400',
    connection_type => 'VISA::GPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.1,
    max_units_per_second => 2,
    min_units => -120,
    max_units => 120,
);

my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'VISA::GPIB',
    connection_options => {pad => 22},
);
$multimeter->sense_nplc(value => '1');