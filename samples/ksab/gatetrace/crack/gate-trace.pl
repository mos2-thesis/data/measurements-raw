#!/usr/bin/perl
#PODNAME: gate-trace.pl
#ABSTRACT: Measure a gate trace

use Lab::Moose;
use 5.010;

# Sample

my $sample = 'KSAB_';  # chip name
my $PINsI = 'Vsd#18#21_';    # pins / cables
my $PINGate = 'Vg#10_';     # pins / cables

# parameters of the setup

my $lineresistance=150; # resistance of measurement line (Ohm)
my $sensitivity = -1e-9; # sensitivity of DL1211 in A/V
my $risetime = 0.100;       # rise time in s

my $biasvoltage = 0.1;   # voltage set to biasyokogawa
my $divider = 100;      # voltage divider

my $samplebias = ($biasvoltage/$divider);

my $livePlot = 1;
my $NPLC = 1;

# parameters of the gate trace
my $gatestart = 0;
my $gateend = 20;
my $gatestep = 0.001;
my $gateDelay = 0; # s
# my $gateInDelay = 0.5 * $risetime; # s
my $gateInDelay = 0; # s
my $gatebackswp = 0; # Do a backsweep (1) or not (0)

# instruments
my $lakeshore = instrument(
    type => 'Lakeshore340',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 19},
    input_channel => 'A', # set default input channel for all method calls
);
my $TEMP = $lakeshore->get_sensor_units_reading();
 
my $multimeter = instrument(
    type => 'Agilent34410A',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 22},
);
#$multimeter->set_nplc(value => $NPLC);

my $gatesource = instrument(
    type => 'Keithley2400',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 29},
    max_units_per_step => 0.002,
    max_units_per_second => 0.05,
    min_units => -20,
    max_units => 20,
);
$gatesource->sense_function_on(value => ['CURR']);
$gatesource->sense_nplc(value => 0.1);

# biassource
my $biassource = instrument(
    type => 'YokogawaGS200',
    connection_type => 'LinuxGPIB',
    connection_options => {pad => 1},
    max_units_per_step => 0.05,
    max_units_per_second => 0.1,
    min_units => -1,
    max_units => 1,
);

# sweep
my $sweep = sweep(
    type       => 'Step::Voltage',
    instrument => $gatesource,
    from => $gatestart, to => $gateend, step => $gatestep,
    backsweep => $gatebackswp,
	delay_before_loop => $gateDelay,
);

# data file

my $datafile = sweep_datafile(columns => [qw/voltage current gatecurrent/]);

# plot 
$datafile->add_plot(
	x => 'voltage',
	y => 'current',
	plot_options => {
		title => "Gatesweep - $sample$PINsI - $PINGate - V_Bias = $samplebias V - T = $TEMP Ohm",
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
	curve_options => {
		with => 'lines',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	},
	live => $livePlot,
);

# #gatecurrent plot
$datafile->add_plot(
	x => 'voltage',
	y => 'gatecurrent',
	plot_options => {
		title => "Gatesweep - $sample$PINsI - $PINGate - V_Bias = $samplebias V - T_MCO2 = $TEMP mK",
		format => {x => "'%.1f'", y => "'%.2e'"},
		grid => 0,
	},
	curve_options => {
		with => 'lines',
		linetype => 2,
		linecolor => 'black',
		linewidth => 2,
	},
	hard_copy_suffix => '_gatecurrent',
	live => $livePlot,
);

# measurement
my $meas = sub {
  my $sweep = shift;
 
  my $ampvoltage = $multimeter->get_value();
  my $i_dc = $ampvoltage*$sensitivity; 
  my $gatecurrent = $gatesource->get_measurement()->{CURR};
 
  $sweep->log(
	  voltage => $gatesource->cached_level(),
	  current => $i_dc,
	  gatecurrent => $gatecurrent,
  );
};

# run it
$biassource->set_level(value => $biasvoltage);
$sweep->start(
    measurement => $meas,
    datafile    => $datafile,
    folder		=> $sample.$PINsI.$PINGate.'gatetrace',
    date_prefix => 1,
);

# go back to zero, if backsweep is disabled
$gatesource->set_level(value => 0);
$biassource->set_level(value => 0);
